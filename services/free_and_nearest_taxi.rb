require './clients/cabify.rb'

module Services
  class FreeAndNearestTaxi
    include Math

    def free_taxi(latitude, longitude)
      taxis = Clients::Cabify.new.taxis

      nearest_taxi(latitude, longitude, taxis)
    end

    private

    def nearest_taxi(latitude, longitude, taxis)
      taxis.each_with_object({}) do |taxi, hash|
        distance = calculate_distance(latitude, longitude, taxi.dig('location', 'lat'), taxi.dig('location', 'lon'))

        if hash['name'].nil? || hash['distance'] > distance
          hash['name'] = taxi['name']
          hash['city'] = taxi['city']
          hash['distance'] = distance
        end
      end
    end

    def calculate_distance(lat_1, long_1, lat_2, long_2)
      (
        6371 *
        acos(
          cos(radians(lat_1)) * cos(radians(lat_2)) * cos(radians(long_2) -
          radians(long_1)) +
          sin(radians(lat_1)) * sin(radians(lat_2))
        )
      ).round(1)
    end

    def radians(degrees)
      degrees * Math::PI / 180
    end
  end
end
