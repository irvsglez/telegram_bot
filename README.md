# Cabify Telegram Bot

This is a bot for Telegram allowing a user to request THE NEAREST AVAILABLE TAXI TO A CONCRETE LOCATION.

# How to run it
1. `bundle install`
2. `ruby app.rb`
3. From you smartphone look for _booking_taxi_ bot.
4. Type `/help` to see available command.
If you want to run the specs: `bundle exec rspec spec/`

## Considerations
* Available commands:
  * `/nearest_taxi`. Returns the nearest free taxi given user's location.
  * `/book city model`. Books a specific taxi.
  * `/book_location latitude longitude`. Books a taxi given a specific location (latitude, longitude).
  * `/is_booked city model`. Checks if a taxi is booked.

Keeping in mind that the service is public, and anyone can be using the API at any moment, I have mocked API requests in the tests.

# Problems
`POST /taxis/<city>/<taxi_id>` This endpoints has been down since I received the challenge, so I could not use it. I hope the app behaves as expected.
