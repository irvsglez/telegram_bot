# You must define which commands will be used to request the taxi,
# and to validate that is has been correctly ordered.

# GET /taxis -> Returns all the taxis in the system
# GET /taxis/<city>/ -> Returns all the free taxis in the city
# GET /taxis/<city>/<taxi_id> -> Returns the information of a taxi
# POST /taxis/<city>/<taxi_id>
# {state: hired}
# If the requested taxi is available the service will respond 200 OK,
# and 403 FORBIDDEN if that's not the case.
require 'telegram/bot'
require 'json'
include Math

def token
  '735480753:AAFWZzcE9JVi3iKC9N6JKm77y9CUKwbBmf4'
end

def telegram_bot
  Telegram::Bot::Client.run(token) do |bot|
    bot.listen do |message|
      case message.text
      when '/free_taxis'
        bot.api.send_message(chat_id: message.chat.id, text: free_taxis, parse_mode: 'Markdown')
      when '/nearest_taxi'
        bot.api.send_message(chat_id: message.chat.id, text: taxi_distances)
      when '/detail'
        bot.api.send_message(chat_id: message.chat.id, text: 'Detail')
      when '/book'
        bot.api.send_message(chat_id: message.chat.id, text: 'Booking')
      when '/map'
        bot.api.send_location(chat_id: message.chat.id, latitude: 38.41, longitude: 1.2313)
      end
    end
  end
end

def free_taxis
  "```#{JSON.pretty_generate(JSON.parse(get_taxis))}```"
end

# ¿cómo recorro el json para coger todos los nodos?
def taxis_names
  response = JSON.parse(get_taxis)
  names = []
  response.each do |taxi|
    names << taxi['name']
  end
  names.to_s
end

def taxi_locations
  response = JSON.parse(get_taxis)
  locations = []
  response.each do |taxi|
    locations << "#{taxi['location']['lat']} #{taxi['location']['lon']}"
  end
  locations.to_s
end

def taxi_distances
  response = JSON.parse(get_taxis)
  taxi_distance = []

  response.each do |taxi|
    distance = calculate_distance(40.416641, -3.703832, taxi['location']['lat'], taxi['location']['lon'])
    taxi_distance << { name: "#{taxi['name']}", distance: "#{distance}" }
  end
  nearest_taxi(taxi_distance)
  # taxi_distance.to_s
end

def nearest_taxi(taxi_distances)
  nearest_distance = taxi_distances.first['distance'].to_i
  nearest_taxi = ''
  taxi_distances.each do |name, distance|
    if distance.to_i <= nearest_distance
      nearest_distance = distance.to_i
      nearest_taxi = "#{name} #{distance}"
    end
  end
  nearest_taxi.to_s
end

def get_taxis
  `curl -X GET "http://130.211.103.134:4000/api/v1/taxis"`
end

def calculate_distance(lat_1, long_1, lat_2, long_2)
  6371 *
    acos(
      cos(radians(lat_1)) * cos(radians(lat_2)) * cos(radians(long_2) -
      radians(long_1)) +
      sin(radians(lat_1)) * sin(radians(lat_2))
    )
end

def radians(degrees)
  degrees * Math::PI / 180
end

begin
  puts calculate_distance(40.416641, -3.703832, 41.387001, 2.170058)
  telegram_bot
rescue => e
  puts "ERROR: #{e}"
end
