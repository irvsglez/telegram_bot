require 'httparty'

module Clients
  class Cabify
    URL = 'http://130.211.103.134:4000/api/v1'

    def taxis
      HTTParty.get(URL + '/taxis')
    end

    def taxi(city, name)
      HTTParty.get(URL + "/taxis/#{city}/#{name}")
    end

    def book_taxi(city, name)
      HTTParty.post(URL + "/taxis/#{city}/#{name}")
    end
  end
end
