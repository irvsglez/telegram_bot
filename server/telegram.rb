require 'telegram/bot'
require './services/free_and_nearest_taxi.rb'
require './clients/cabify'

module Server
  class TelegramBot
    TELEGRAM_TOKEN = '735480753:AAFWZzcE9JVi3iKC9N6JKm77y9CUKwbBmf4'

    def start
      Telegram::Bot::Client.run(TELEGRAM_TOKEN) do |bot|
        bot.listen do |message|
          unless message.location.nil?
            taxi = Services::FreeAndNearestTaxi.new.free_taxi(message.location.latitude, message.location.longitude)

            text =  unless taxi['name'].nil?
                      "Your taxi model is #{taxi['name']} and it is #{taxi['distance']} km from you in #{taxi['city']}. To book it, write /book #{taxi['city']} #{taxi['name']}"
                    else
                      'Impossible to find a taxi close to the location.'
                    end
            bot.api.send_message(chat_id: message.chat.id, text: text, parse_mode: 'Markdown')
          end

          unless message.text.nil?
            command = message.text.split(' ').first
            log_command(command)
          end

          case command
          when '/nearest_taxi'
            kb = Telegram::Bot::Types::KeyboardButton.new(text: 'Show me your location', request_location: true)
            markup = Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: kb)

            bot.api.send_message(chat_id: message.chat.id, text: 'I need your location to find a taxi', reply_markup: markup)
          when '/book'
            if missing_params?(message)
              bot.api.send_message(chat_id: message.chat.id, text: 'Command usage: /book city model')
              next
            end

            taxi = cabify_client.taxi(city, model)

            unless is_still_free?(taxi)
              bot.api.send_message(chat_id: message.chat.id, text: 'Taxi is not available any more. Try to find another.')
              next
            end

            booked_response = cabify_client.book_taxi(city(message), model(message))

            if taxi_booked?(booked_response)
              bot.api.send_message(chat_id: message.chat.id, text: 'Taxi has been booked. Wait for it.')
            else
              bot.api.send_message(chat_id: message.chat.id, text: 'Taxi is not available any more. Try to find another.')
            end
          when '/book_location'
            if missing_params?(message)
              bot.api.send_message(chat_id: message.chat.id, text: 'Command usage: /book_location latitude longitude')
              next
            end

            taxi = Services::FreeAndNearestTaxi.new.free_taxi(latitude(message), longitude(message))
            text =  unless taxi['name'].nil?
                      "The nearest taxi is a model #{taxi['name']} and it is #{taxi['distance']} km from you in #{taxi['city']}. To book it, write /book #{taxi['city']} #{taxi['name']}"
                    else
                      'Impossible to find a taxi close to the location.'
                    end
            bot.api.send_message(chat_id: message.chat.id, text: text, parse_mode: 'Markdown')
          when '/is_booked'
            taxi = cabify_client.taxi(city(message), model(message))
            if is_booked?(taxi)
              bot.api.send_message(chat_id: message.chat.id, text: 'Taxi is booked. Wait for it.')
            else
              bot.api.send_message(chat_id: message.chat.id, text: 'Taxi is not book. Try again.')
            end
          when '/help'
            bot.api.send_message(chat_id: message.chat.id, text: available_commands)
          end
        end
      end
    end

    private
    def cabify_client
      @cabify_client ||= Clients::Cabify.new
    end

    def is_still_free?(taxi)
      taxi['state'] == 'free'
    end

    def taxi_booked?(response)
      response['state'] == 'hired'
    end

    def missing_params?(message)
      message.text.split(' ')[1].nil? || message.text.split(' ')[2].nil?
    end

    def city(message)
      message.text.split(' ')[1]
    end

    def model(message)
      message.text.split(' ')[2]
    end

    def latitude(message)
      message.text.split(' ')[1]
    end

    def longitude(message)
      message.text.split(' ')[2]
    end

    def log_command(command)
      puts "Command received: #{command}"
    end

    def available_commands
      'Available commands: /nearest_taxi | /book city model | /book_location latitude longitude | /is_booked city model'
    end
  end
end
