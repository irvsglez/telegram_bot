require './services/free_and_nearest_taxi'

describe Services::FreeAndNearestTaxi do
  describe '#free_taxi' do
    subject { described_class.new }

    let(:clients_cabify_instance) { double(:klass) }
    let(:ford_taxi) do
      {
       "state" => "free",
       "name" => "Ford",
       "location" => {
         "lon" => 1.2313,
         "lat" => 38.41
       },
       "city" => "Madrid"
      }
    end
    let(:skoda_taxi) do
      {
        "state" => "free",
        "name" => "Skoda4",
        "location" => {
          "lon" => 1.3123,
          "lat" => 38.123
        },
        "city" => "Madrid"
      }
    end
    let(:opel_taxi) do
      {
        "state" => "free",
        "name" => "Opel",
        "location" => {
          "lon" => 1.399,
          "lat" => 38.88
        },
        "city" => "Madrid"
      }
    end
    let(:free_taxis_response) do
      [ford_taxi, skoda_taxi, opel_taxi]
    end
    let(:latitude) { 41.396515 }
    let(:longitude) { 2.195000 }

    before do
      expect(Clients::Cabify)
        .to receive(:new)
        .and_return(clients_cabify_instance)

      expect(clients_cabify_instance)
        .to receive(:taxis)
        .and_return(free_taxis_response)
    end

    it 'returns the nearest and free taxi' do
      expect(subject.free_taxi(latitude, longitude))
        .to eq({ "city"=>"Madrid", "distance"=>287.9, "name"=>"Opel" })
    end
  end
end
