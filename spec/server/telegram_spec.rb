require './server/telegram'

describe Server::TelegramBot do
  describe '#start' do

    let(:telegram_bot_client) { double(:telegram_client) }
    let(:telegram_bot) { double(:bot) }
    let(:bot) do
      Telegram::Bot::Client.new(described_class::TELEGRAM_TOKEN).tap { |x| x.extend described_class }
    end

    before do
      expect(Telegram::Bot::Client)
        .to receive(:run)
        .with(described_class::TELEGRAM_TOKEN)
        .and_return(telegram_bot_client)

      expect(telegram_bot_client)
        .to receive(:listen)
        .and_return(message)
    end

    context 'when message is /taxi' do
      let(:message) { double(text: '/taxi', chat: { id: 1 }) }
      let(:kb) { double(:keyboard_button) }
      let(:markup) { double(:reply_keyboard_markup) }
      let(:bot_api) { double(:bot_api) }

      before do
        expect(Telegram::Bot::Types::KeyboardButton)
          .to receive(:new)
          .with(text: 'Show me your location', request_location: true)
          .and_return(kb)

        expect(Telegram::Bot::Types::ReplyKeyboardMarkup)
          .to receive(:new)
          .with(keyboard: kb)
          .and_return(markup)

        expect(telegram_bot_client)
          .to receive(:api)
          .and_return(bot_api)
      end

      after do
        described_class.new.start
      end

      xit "i do not know what i am doing xD" do
        expect(bot_api)
          .to receive(:send_message)
          .with(chat_id: 1, text: 'I need your location to find a taxi', reply_markup: markup)
      end
    end
    # ...
  end
end

# This suite must also provide consistent results (keep in mind that the service is public,
#  and anyone can be using the API in any moment).
# Por eso uso mocks, decirlo en el email.
