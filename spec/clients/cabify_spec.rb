require './clients/cabify.rb'

describe Clients::Cabify do
  let(:ford_taxi) do
    {
      "state": "free",
      "name": "Ford",
      "location": {
        "lon": 23.23,
        "lat": 22.34
      },
      "city": "Madrid"
    }
  end
  let(:hyundai_taxi) do
    {
      "state": "free",
      "name": "Hyundai",
      "location": {
        "lon": 1.2313,
        "lat": 38.41
      },
      "city": "Madrid"
    }
  end

  describe '#taxis' do
    subject { described_class.new }

    context 'when taxis available' do
      let(:taxis) { [ford_taxi, hyundai_taxi] }

      it 'returns available taxis' do
        expect(HTTParty)
          .to receive(:get)
          .with("#{described_class::URL}/taxis")
          .and_return(taxis)
        expect(subject.taxis).to eq(taxis)
      end
    end

    context 'when taxis not available' do
      let(:no_taxis) { [] }

      it 'does not return any taxis' do
        expect(HTTParty)
          .to receive(:get)
          .with("#{described_class::URL}/taxis")
          .and_return(no_taxis)
        expect(subject.taxis).to eq(no_taxis)
      end
    end
  end

  describe '#taxi' do
    subject { described_class.new }

    context 'when city and model are right' do
      it "returns taxi's info" do
        expect(HTTParty)
          .to receive(:get)
          .with("#{described_class::URL}/taxis/Madrid/#{ford_taxi[:name]}")
          .and_return(ford_taxi)
        expect(subject.taxi('Madrid', ford_taxi[:name])).to eq(ford_taxi)
      end
    end

    context 'when city or model are not right' do
      it "returns null" do
        expect(HTTParty)
          .to receive(:get)
          .with("#{described_class::URL}/taxis/AaaBbb/#{ford_taxi[:name]}")
          .and_return(nil)
        expect(subject.taxi('AaaBbb', ford_taxi[:name])).to eq(nil)
      end
    end
  end

  describe '#book_taxi' do
    subject { described_class.new }
    let(:taxi_status) { { 'state': 'hired' } }

    context 'when city and model are right' do
      it 'book the taxi' do
        expect(HTTParty)
          .to receive(:post)
          .with("#{described_class::URL}/taxis/Madrid/#{ford_taxi[:name]}")
          .and_return(taxi_status)
        expect(subject.book_taxi('Madrid', ford_taxi[:name])).to eq(taxi_status)
      end
    end
  end
end
